# Spring Boot JPA MySQL - Building Rest CRUD API example

## Install
- Faites "java -version". Si cela ne fonctionne pas, installez java : https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.exe
- Installer maven : https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip (unzip dans un répertoire de dev), ajouter la variable M2_HOME et ajouter au PATH (cf :https://howtodoinjava.com/maven/how-to-install-maven-on-windows/). 
- Tester si maven fonctionne : "mvn --version"
- Installer Mysql 8 si ce n'est pas déjà fait
- Modifier le fichier : src/main/resources/application.properties


## Docker cmd
Note : pensez à cloner les gits front et back dans un dossier commun.

build :
- mysql : docker pull mysql:latest
- adminer : docker pull adminer
- Spring : docker build . -t "deploytpback"

run :
- mysql : docker run --name mysqldb -e MYSQL_ROOT_PASSWORD=example -e MYSQL_DATABASE=db -p 3306:3306 -v ./dbRestor:/var/lib/mysql -d mysql:latest
- adminer : docker run --name adminer -p 8081:8080 -d adminer:latest
- Spring : docker run --name backend -e SPING_DATASOURCE_PASSWORD=**** -e SPRING_DATASOURCE_USERNAME=****** -e SPRING_DATASOURCE_URL="jdbc:mysql://mds2.cdxwmy7n2r5p.eu-west-3.rds.amazonaws.com:3306/mathieu_exam" -p 8080:8080 -d deploytpback:latest

compose :

Dans un shell :
- cd <pathToCloneFolder>\mds-dds-exam-spring-boot-serve
- docker-compose up

Ouvrir un navigateur web et allez sur http://localhost:80

# Deploy Heroku by Dockerfile

- git add .
- git commit -m "text of commit"
- heroku stack:set container
- git push heroku main