FROM openjdk:18-jdk-alpine3.15
VOLUME /tmp
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src
RUN chmod +x mvnw
RUN ./mvnw install -DskipTests
ENTRYPOINT ["java","-jar", "-Dserver.port=$PORT","/root/.m2/repository/io/helyx/mds-dds-exam-spring-boot-server/0.0.1-SNAPSHOT/mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar"]